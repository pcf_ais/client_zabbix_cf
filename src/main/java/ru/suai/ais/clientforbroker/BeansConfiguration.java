package ru.suai.ais.clientforbroker;

import org.springframework.context.annotation.Configuration;
import ru.suai.ais.zabbixclient.AutoConfigurationClientZabbix;

/**
 * @author rost.
 */
@Configuration
public class BeansConfiguration extends AutoConfigurationClientZabbix{
}
