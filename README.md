## Обзор
Клиентское приложение для соединения с Service Broker Zabbix

## Установка

Создать экземпляр сервиса брокера Zabbix.
```
cf create-service example-broker-zabbix client name-broker-instance
```
Указать в manifiest.yml в корне проекта:
```
applications:
- name: name-broker-instance
  services:
  - service-instance
path: target/AppsClientForServiceBroker-1.0-SNAPSHOT.jar
```
Запушить приложение:
