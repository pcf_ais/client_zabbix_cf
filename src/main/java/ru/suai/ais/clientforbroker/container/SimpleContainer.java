package ru.suai.ais.clientforbroker.container;

import org.springframework.cloud.cloudfoundry.com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by rost on 22.11.17.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class SimpleContainer {
    int id;
    String str;

    public SimpleContainer(){}

    public SimpleContainer(int id, String str){
        this.id = id;
        this.str = str;
    }

    public int getId(){return id;}
    public String getStr(){    return str;}

    public void setId(int id){this.id = id;}
    public void setStr(String str){this.str = str;}
}
