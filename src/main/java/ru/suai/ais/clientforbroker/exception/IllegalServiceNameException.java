package ru.suai.ais.clientforbroker.exception;

/**
 * Created by rost on 22.11.17.
 */
public class IllegalServiceNameException extends RuntimeException {
    public IllegalServiceNameException(String service){
        super("Can't find this service:" + service);
    }

    public IllegalServiceNameException(){
        super("Can't find offered service");
    }
}
