package ru.suai.ais.clientforbroker.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import ru.suai.ais.zabbix.NewItemParams;
import ru.suai.ais.zabbixclient.ClientBroker;
import ru.suai.ais.zabbixclient.metadata.ServiceBrokerInfoZabbix;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rost on 06.12.17.
 */
@Controller
public class WebController extends WebMvcConfigurerAdapter {
    @Autowired
    ServiceBrokerInfoZabbix zabbixInfoClient;
    @Autowired
    ClientBroker clientBroker;

    /**
     * Simple Web Page<br>
     * it's not necessary
     * */
    @RequestMapping("/")
    public String showForm(Model model){
        model.addAttribute("urlSend", zabbixInfoClient.getUrl());
        model.addAttribute("appID", zabbixInfoClient.getAppId());
        model.addAttribute("serviceName", zabbixInfoClient.getServiceName());
        model.addAttribute("values", HomeController.listKeys);
        return "form";

    }

    /**
     * register User-provided items
     *
     * @param name this is the new name of item
     * */
    @RequestMapping("/test/registerFromWeb")
    String registerMyOwnItems(Model model, @RequestParam(name = "name")String name){
        List<String>list = new ArrayList<String>();
        list.add(name);
        NewItemParams newItemParams = (NewItemParams)clientBroker.registerItems(list);
        HomeController.listKeys.add(newItemParams);
        return "redirect:/";
    }

}
