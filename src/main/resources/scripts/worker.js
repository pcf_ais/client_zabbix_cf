var wrapper = document.querySelector(".wrapper");
var counter = 0;
document.querySelector(".add-button").addEventListener("click",function(e) {
    e.preventDefault();
    counter++;
    var div = document.createElement("div");
    div.className = "form-group form-inline";
    var label = document.createElement("label");
    label.innerHTML = "Name of item";
    var input = document.createElement("form:text");
    input.className = "form-control";
    input.placeholder="Name of item";
    input.setAttribute("data-id", counter);
    input.name = "store.names[" + counter + "]";
    input.setAttribute("th:field", "*{names[" + counter + "]}");
    var button = document.createElement("button");
    button.className = "btn btn-danger remove-button";
    button.innerHTML = "&nbsp;<span class='glyphicon glyphicon-minus'></span>&nbsp;"
    button.addEventListener("click", removeRow);
    div.appendChild(label);
    div.appendChild(input);
    div.appendChild(button);
    wrapper.appendChild(div);
});

document.querySelector(".remove-button").addEventListener("click", removeRow);


function removeRow(e) {
  e.preventDefault();
  counter--;
  var el = e.target;
  var deletedIndex = null;
  //go up
  while (el.tagName != "DIV") {
    el = el.parentNode;
  }
  for (var i = 0; i < el.children.length; i++) {
    var child = el.children[i];
    if(child.tagName == "INPUT") {
      deletedIndex = child.getAttribute("data-id"); // console.log(child.getAttribute("data-id"));
    }
  }

  var allInputs = document.querySelectorAll(".form-group input");

  for (var i = 0; i < allInputs.length; i++) {
    var curInput = allInputs[i];
    var curIndex = curInput.getAttribute("data-id");
    if (deletedIndex < curIndex) {
      curInput.setAttribute("data-id", curIndex - 1)
      curInput.name = "names[" + (curIndex - 1) + "]"
    }
    //console.log(curIndex);
  }
  el.parentNode.removeChild(el);
}
