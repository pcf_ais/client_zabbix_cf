package ru.suai.ais.clientforbroker;

/**
 * Created by rost on 17.11.17.
 * if you want see about http answer:
 * {@link ru.suai.ais.clientforbroker.controller.HomeController}
 *
 */
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan
@EnableAutoConfiguration
public class Application {
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
