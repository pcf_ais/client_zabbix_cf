package ru.suai.ais.clientforbroker.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.suai.ais.zabbixclient.ClientBroker;
import ru.suai.ais.zabbixclient.metadata.NewItemParams;
import ru.suai.ais.zabbixclient.metadata.ServiceBrokerInfoZabbix;
import ru.suai.ais.zabbixclient.metadata.ZabbixInfoClient;

import java.util.*;

/**
 * Created by rost on 20.11.17.
 */

@Configuration
@RestController
public class HomeController {
    /**
     * Information about Service Broker
     * @see ServiceBrokerInfoZabbix
     * */
    @Autowired(required = false)
    ServiceBrokerInfoZabbix serviceBrokerInfoZabbix;

    /**
     * List registered items
     *
     * */
    public static List<Object>listKeys = new LinkedList();

    /**
     * This is worker with service broker
     * */
    @Autowired
    ClientBroker clientBroker;

    /**
     * return some parametr for data sender
     * <br>example answer:
     * <pre>
     *  {
     *      "url":"tcp.apps.testpcf.org",
     *      "appId":"ed8daf12-2c98-4768-8277-845bbe59993b",
     *      "values":[
     *          {
     *              "delay":30,
     *              "hostid":"10282",
     *              "interfaceid":"30",
     *              "key":"key-1379710846",
     *              "name":"Dell-GW",
     *              "type":2,
     *              "valueType":0
     *           }
     *      ],
     *      "idHost":"10282",
     *      "numPort":"10051",
     *      "lengthValues":1
     *  }
     * </pre>
     *
     * @return data about Zabbix
     * */
    @RequestMapping("/test/gettestinfo")
    ZabbixInfoClient getSomeTestInfo(){
        ZabbixInfoClient zabbixInfoClient = clientBroker.getInfoFromBroker();
        zabbixInfoClient.setValues(listKeys);
        return (zabbixInfoClient);
    }
    /**
     * get parametr to communicte with Service Broker<br>
     *     need to test<br>
     *     example json:
     * <pre>
     *     {
     *      "serviceName":"my-broker",
     *      "url":"appsservicebroker.apps.testpcf.org",
     *      "appId":"ed8daf12-2c98-4768-8277-845bbe59993b",
     *      "userInfoZabbix":
     *          {
     *              "username":"user",
     *              "password":"0d4435a4-fecd-434a-b798-fc83d9ade3db"
     *              }}
     * </pre>
     * @return info about service broker
     * */
    @RequestMapping("/test/settings")
    ServiceBrokerInfoZabbix getServiceBrokerInfoZabbix(){
        return serviceBrokerInfoZabbix;
    }

    /**
     * register some items for zabbix sender
     * @return NewItemParams information about items
     * @see NewItemParams
     * */
    @RequestMapping("/test/registerItems")
    Object registerMyOwnItems(@RequestParam(name = "name")String name){
        List<String>list = new ArrayList<String>();
        list.add(name);
        NewItemParams newItemParams = (NewItemParams)clientBroker.registerItems(list);
        this.listKeys.add(newItemParams);
        return newItemParams;
    }

    /**
     * Register Items and get info about data send
     * <br> the same as previous method
     * @see HomeController#registerMyOwnItems(String)
     * @see HomeController#registerMyOwnItems(String)
     * */
    @RequestMapping("/test/getAndRegister")
    Object getAndRegisterItems(@RequestParam(name = "name")String name){
        Map<Object, Object>map = new TreeMap<Object, Object>();
        map.put("items_key", ((NewItemParams)this.registerMyOwnItems(name)).getKey());
        map.put("some_info", this.getSomeTestInfo());

        return map;
    }


}
