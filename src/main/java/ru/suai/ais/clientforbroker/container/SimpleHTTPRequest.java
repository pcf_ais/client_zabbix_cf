package ru.suai.ais.clientforbroker.container;


import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 *
 * @author AIS
 */
public class SimpleHTTPRequest {

    /**
     * Sends request to server
     *
     * @param urlStr URL string
     *
     * @param requestMethod HttpRequest method - "GET" - "POST"
     *
     * @param requestParam Requset parameters
     *
     * @return string response
     */
    public static String send(String urlStr, String requestMethod, String requestParam) {
        HttpURLConnection connection = null;
        String line;
        StringBuilder response = new StringBuilder();
        try {
            connection = (HttpURLConnection) new URL(urlStr).openConnection();
            connection.setRequestMethod(requestMethod);
            connection.addRequestProperty("User-Agent", "");
            connection.addRequestProperty("Content-Type", "application/json-rpc");
            connection.setDefaultUseCaches(false);
            connection.setDoOutput(true);
            connection.connect();

            PrintStream out = new PrintStream(connection.getOutputStream());
            out.print(requestParam);
            out.flush();
            out.close();

            BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            if (connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                while ((line = in.readLine()) != null) {
                    response.append(line);
                    response.append('\n');
                }
            } else {
                System.out.println("Error: " + connection.getResponseCode());
            }
            in.close();
        } catch (Throwable err) {
            err.printStackTrace();
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
        return response.toString();
    }
}

